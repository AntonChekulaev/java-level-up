package services;

import exceptions.MyException;
import main.DatabaseConnection;
import model.Account;
import model.User;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import org.apache.log4j.Logger;

/**
 * Service gets crud operations for working with Account model
 * @author Anton Chekulaev
 * @version 1.0
 * @see model.Account
 */
public class AccountManager {

    /** DatabaseConnection variable for connection to postgres database */
    DatabaseConnection connection;

    /** Constructor - run a connection to postgres database method*/
    public AccountManager() {
        connection = new DatabaseConnection();
    }

    /**
     * Method create a new account using user id
     * @param id
     * @throws SQLException
     * @throws IOException
     */
    public void addAccount(int id) throws SQLException, IOException {
        User currentUser = getUser(id);
        if (currentUser != null) {
            Account account = new Account();
            account.setUserId(currentUser.getId());
            account.setAccountMoney(0.0);
            String query = "INSERT INTO accounts (userId, accountMoney) VALUES (\'"
                    + account.getUserId()
                    + "\', \'" + account.getAccountMoney()
                    + "\')";

            sqlExec(query);
        }
    }

    /**
     * Method delete a existing account
     * @param accountNumber
     * @throws SQLException
     */
    public void deleteAccount(int accountNumber) throws SQLException {
        String query = "DELETE FROM accounts WHERE accountNumber = " + accountNumber;
        sqlExec(query);
    }

    /**
     * Method add a money to account score
     * @param accountNumber
     * @param money
     * @throws SQLException
     */
    public void changeAccount(int accountNumber, double money) throws SQLException {
        String query = "UPDATE accounts SET accountMoney = accountMoney + " + money
                + "WHERE accountNumber = " + accountNumber;
        sqlExec(query);
    }

    /**
     * Method get accounts of existing user
     * @param id
     * @return account of user by id
     * @throws SQLException
     */
    public List<Account> getUserAccounts(int id) throws SQLException {
        List<Account> listAccounts = new ArrayList<>();
        String query = "SELECT * FROM accounts WHERE userId = " + id;
        Statement statement = connection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            Account account = new Account();
            account.setAccountNumber(resultSet.getInt("accountNumber"));
            account.setUserId(resultSet.getInt("userId"));
            account.setAccountMoney(resultSet.getDouble("accountMoney"));
            listAccounts.add(account);
        }
        return listAccounts;
    }

    /**
     * Method get all users with  their accounts
     * @return map of users
     * @throws SQLException
     */
    public Map<Integer, User> getInformationAboutUsers() throws SQLException {
        Map<Integer, User> infoUsers = convertListUsersToMap
                (readUsersWithAccounts(readUsers(), readAccounts()));
        return infoUsers;
    }

    // Support classes

    /**
     * Method execute the sql request
     * @param query
     * @throws SQLException
     */
    private void sqlExec (String query) throws SQLException {
        Statement statement = connection.getConnection().createStatement();
        statement.execute(query);
        connection.getConnection().close();
    }

    /**
     * Method get user by user id
     * @param id
     * @return User model
     * @throws SQLException
     * @throws IOException
     */
    public User getUser(int id) throws SQLException, IOException {
        User user = new User();
        String query = "SELECT * FROM users WHERE id = " + id;
        Statement statement = connection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
         if (resultSet.next()) {
             user.setId(resultSet.getInt("id"));
             user.setName(resultSet.getString("name"));
             user.setSurname(resultSet.getString("surname"));
         } else {
             throw new SQLException("Нет такого пользователя");
         }
        connection.getConnection().close();

        List<Account> listAccounts = getUserAccounts(id);

        user.setAccount(listAccounts);
        return user;
    }

    /**
     * Method convert List of users to map<Integer, User>,
     * where integer is id of user
     * @param listUsers
     * @return map of Users
     */
    private Map<Integer, User> convertListUsersToMap(List<User> listUsers) {
        Map<Integer, User> info = new HashMap<Integer, User>();
        for (User user : listUsers) {
            info.put(user.getId(), user);
        }
        return info;
    }

    //read methods

    /**
     * Method connects accounts with their users
     * @param listUsers
     * @param listAccounts
     * @return List of users with accounts
     */
    private List<User> readUsersWithAccounts(List<User> listUsers, List<Account> listAccounts) {
        List<Account> userListAccounts = new ArrayList<>();
        for (User user : listUsers) {
            for (Account account : listAccounts) {
                if (user.getId() == account.getUserId()) {
                    userListAccounts.add(account);
                }
            }
            user.setAccount(userListAccounts);
            userListAccounts = new ArrayList<>();
        }
        return listUsers;
    }

    /**
     * Method get users from database
     * @return List of users without accounts
     * @throws SQLException
     */
    private List<User> readUsers() throws SQLException {
        List<User> listUsers = new ArrayList<>();

        String query = "SELECT * FROM users";

        Statement statement = connection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setSurname(resultSet.getString("surname"));
            listUsers.add(user);
        }

        connection.getConnection().close();

        return listUsers;
    }

    /**
     * Method get account from database
     * @return List of accounts
     * @throws SQLException
     */
    private List<Account> readAccounts() throws SQLException {
        List<Account> listAccounts = new ArrayList<>();
        String query = "SELECT * FROM accounts";
            Statement statement = connection.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Account account = new Account();
                account.setAccountNumber(resultSet.getInt("accountNumber"));
                account.setUserId(resultSet.getInt("userId"));
                account.setAccountMoney(resultSet.getDouble("accountMoney"));
                listAccounts.add(account);
            }

            connection.getConnection().close();
        return listAccounts;
    }

}
