package services;

import org.apache.log4j.Logger;

/**
 * Service greetings user
 * @author Anton Chekulaev
 * @version 1.0
 */
public class SecurityGuard{

    /** Constructor - run a talk method*/
    public SecurityGuard() {
        talk();
    }

    /**
     * Method get a greeting speech
     * @return String message
     */
    public String talk() {
        return "Привет, ты находишься в банке \" МегаБанк\", "
                + "если хочешь зарегистрироваться, пройди к менеджеру";
    }

}
