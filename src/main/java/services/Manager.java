package services;


import main.DatabaseConnection;
import model.Account;
import model.User;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Service gets crud operations for working with Users model
 * @author Anton Chekulaev
 * @version 1.0
 * @see model.User
 */
public class Manager {

    /** DatabaseConnection variable for connection to postgres database */
    DatabaseConnection connection;

    /** Constructor - run a connection to postgres database method*/
    public Manager() {      
        this.connection = new DatabaseConnection();
    }

    /**
     * Method create a user from postgres database
     * @param name
     * @param surname
     * @throws SQLException
     */
    public void createUser(String name, String surname) throws SQLException {
        String query = "INSERT INTO users (name, surname) VALUES (\'" + name
                + "\', \'" + surname + "\')";
        sqlExec(query);
    }

    /**
     * Method delete a user from postgres database
     * @param id
     * @throws SQLException
     */
    public void deleteUser(int id) throws SQLException {
        if (getUserAccounts(id).isEmpty()) {
            String query = "DELETE FROM users WHERE id = " + id;
            sqlExec(query);
        } else {
            throw new SQLException("У данного пользователя остались аккаунты, \n" +
                    "удалите все аккаунты перед удалением пользователя");
        }
    }

    /**
     * Method update a user from postgres database
     * @param id
     * @param name
     * @param surname
     * @throws SQLException
     */
    public void updateUser(int id, String name, String surname) throws SQLException {
        String query = "UPDATE users SET name = \'" + name +
                "\', surname = \'" + surname + "\' WHERE id = " + id;
        sqlExec(query);
    }

    /**
     * Method read all users from postgres database
     * @return
     * @throws SQLException
     */
    public List<User> readUsers() throws SQLException{
        String query = "SELECT * FROM users";
        List<User> listUsers = new ArrayList<>();
        Statement statement = connection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setName(resultSet.getString("name"));
            user.setSurname(resultSet.getString("surname"));
            listUsers.add(user);
        }
        connection.getConnection().close();
        return listUsers;
    }

    /**
     * Method execute the sql request
     * @param query
     * @throws SQLException
     */
    private void sqlExec(String query) throws SQLException {
        Statement statement = connection.getConnection().createStatement();
        statement.execute(query);
        connection.getConnection().close();
    }

    /**
     * Method read accounts by user id from postgres database
     * @param id
     * @return List of user accounts
     * @throws SQLException
     */
    public List<Account> getUserAccounts(int id) throws SQLException {
        List<Account> listAccounts = new ArrayList<>();
        String query = "SELECT * FROM accounts WHERE userId = " + id;
        Statement statement = connection.getConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            Account account = new Account();
            account.setAccountNumber(resultSet.getInt("accountNumber"));
            account.setUserId(resultSet.getInt("userId"));
            account.setAccountMoney(resultSet.getDouble("accountMoney"));
            listAccounts.add(account);
        }
        return listAccounts;
    }

}
