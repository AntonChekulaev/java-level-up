package views;

import org.apache.log4j.Logger;
import services.AccountManager;
import services.Manager;
import services.SecurityGuard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Use to provide a Main CLI
 * @author Anton Chekulaev
 * @version 1.0
 * @see main.MainClass
 */
public class UIMain {

    /**  Object for reading command line */
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    /**  Logging  */
    private static final Logger log = Logger.getLogger(UIMain.class);

    /** Constructor - run a command interface method */
    public UIMain() {
        run();
    }

    /**
     * Method get a command line interface for Main
     */
    private void run() {
        while (true) {
            log.info("1 - Поговорить с охранником");
            log.info("2 - Поговорить с менеджером");
            log.info("3 - Поговорить с менеджером по счетам");

            log.info("Введите число: ");
            int number = 0;
            try {
                number = Integer.parseInt(bufferedReader.readLine());
            } catch (IOException e) {
                log.info("Некорректный ввод");
            }

            switch (number) {
                case 1:
                    UISecurityGuard security = new UISecurityGuard();
                    break;
                case 2:
                    UIManager manager = new UIManager();
                    break;
                case 3:
                    UIAccountManager accountManager = new UIAccountManager();
                    break;
            }

        }
    }

}
