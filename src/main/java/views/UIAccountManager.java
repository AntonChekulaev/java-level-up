package views;

import exceptions.MyException;
import model.Account;
import model.User;
import org.apache.log4j.Logger;
import services.AccountManager;
import services.Manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Use to provide a AccountManager CLI
 * @author Anton Chekulaev
 * @version 1.0
 * @see services.AccountManager
*/
public class UIAccountManager {

    /**  Object for reading command line */
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    /**  Classes composition with AccountManager  */
    AccountManager accountManager = new AccountManager();

    /**  Logging  */
    private static final Logger log = Logger.getLogger(UIAccountManager.class);

    /** Constructor - run a command interface method */
    public UIAccountManager() {
        hello();
    }

    /**
     * Method get a command line interface for AccountManager
     */
    private void hello() {
        log.info("Добрый день, я аккаунт менеджер, "
                + "чем могу вам помочь?");
        log.info("1 - Открыть счёт");
        log.info("2 - Удалить счёт");
        log.info("3 - Положить средства на счёт");
        log.info("4 - Посмотреть профиль");
        log.info("5 - Посмотреть все профили");

        log.info("Введите число: ");
        int numberAccountManager = 0;
        try {
            numberAccountManager = Integer.parseInt(bufferedReader.readLine());
        } catch (IOException e) {
            log.error("Ошибка при работе с командной строкой - " + e.getMessage());
        }

        switch (numberAccountManager) {
            case 1:
                try {
                    create();
                } catch (SQLException | IOException e) {
                    log.error("Ошибка в создании аккаунта - " + e.getMessage());
                }
                break;
            case 2:
                try {
                    delete();
                } catch (SQLException | IOException e) {
                    log.error("Ошибка в удалении аккаунта - " + e.getMessage());
                }
                break;
            case 3:
                log.info("Введите количество внесённых средств в рублях: ");
                try {
                    update();
                } catch (SQLException | IOException e) {
                    log.error("Ошибка в изменении аккаунта - " + e.getMessage());
                }
                break;
            case 4:
                try {
                    read();
                } catch (IOException | SQLException e) {
                    log.error("Ошибка в просмотре аккаунта - " + e.getMessage());
                }
                break;
            case 5:
                try {
                    readAll();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /**
     * Command line interface for create account function
     * @throws SQLException
     * @throws IOException
     */
    private void create() throws SQLException, IOException {
        log.info("Введите id пользователя");
        int id = Integer.parseInt(bufferedReader.readLine());
        accountManager.addAccount(id);
    }

    /**
     * Command line interface for delete account function
     * @throws SQLException
     */
    public void delete() throws SQLException, IOException {
        Account delAccount = null;
        log.info("Введите номер счёта");
        int accountNumber = Integer.parseInt(bufferedReader.readLine());
        accountManager.deleteAccount(accountNumber);
    }

    /**
     * Command line interface for update account function
     * @throws SQLException
     */
    private void update() throws SQLException, IOException {
        log.info("Введите номер счёта");
        int accountNumber = Integer.parseInt(bufferedReader.readLine());
        log.info("Введите сумму, которую хотите вложить");
        double accountMoney = Double.parseDouble(bufferedReader.readLine());
        accountManager.changeAccount(accountNumber, accountMoney);
    }

    /**
     * Command line interface for read accounts function
     * @throws SQLException
     * @throws IOException
     */
    public void read() throws SQLException, IOException {
        int count = 0;
        log.info("Введите id пользователя");
        int id = Integer.parseInt(bufferedReader.readLine());
        User currentUser = accountManager.getUser(id);
        List<Account> listAccounts = accountManager.getUserAccounts(id);
        if (listAccounts.isEmpty()) {
            log.info("Профиль пользователя " + currentUser.getSurname() + " :");
            log.info("У пользователя нет аккаунтов");
        } else {
            for (Account account : listAccounts) {
                if (id == account.getUserId()) {
                    count = count + 1;
                    log.info("Профиль пользователя " + currentUser.getSurname() + " :");
                    log.info("Номер аккаунта - " + account.getAccountNumber());
                    log.info("Денежные средства на аккаунте - " + account.getAccountMoney());
                }
            }
        }
    }

    /**
     * Command line interface for read all users accounts function
     * @throws SQLException
     */
    public void readAll() throws SQLException {
        Map<Integer, User> infoUsers = accountManager.getInformationAboutUsers();
        log.info("Список всех пользователей: ");
        infoUsers.forEach((key, value) -> {
            log.info("");
            log.info("Пользователь - id = " + value.getId() +
                    ", Имя = " + value.getName() + ", Фамилия = " + value.getSurname());
            log.info("Счета этого пользователя");
            if (value.getAccount() == null) {
                log.info("У этого пользователя нет счетов");
            } else {
                for(Account account : value.getAccount()) {
                    log.info("Номер счёта - " + account.getAccountNumber());
                    log.info("Количество денежных средств - " + account.getAccountMoney());
                }
            }
            log.info("");
        });
    }


}
