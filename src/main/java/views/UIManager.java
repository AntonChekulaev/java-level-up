package views;

import model.User;
import org.apache.log4j.Logger;
import services.Manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * Use to provide a Manager CLI
 * @author Anton Chekulaev
 * @version 1.0
 * @see services.Manager
 */
public class UIManager {

    /**  Classes composition with Manager  */
    Manager manager = new Manager();

    /**  Logging  */
    private static final Logger log = Logger.getLogger(UIManager.class);

    /**  Object for reading command line */
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    /** Constructor - run a command interface method */
    public UIManager() {
        hello();
    }

    /**
     * method get a command interface for Manager
     */
    public void hello() {
        log.info("1 - Зарегистрироваться в банке");
        log.info("2 - Снять регистрацию с пользователя");
        log.info("3 - Изменить данные");
        log.info("4 - Посмотреть список пользователей");
        log.info("Введите число: ");
        int numberManager = 0;
        try {
            numberManager = Integer.parseInt(bufferedReader.readLine());
        } catch (IOException e) {
            log.error("Ошибка при введении значения - " + e.getMessage());
        }
        switch (numberManager) {
            case 1:
                try {
                    create();
                } catch (IOException | SQLException e) {
                    log.error("Ошибка в создании пользователя - " + e.getMessage());
                }
                break;
            case 2:
                try {
                    delete();
                } catch (SQLException | IOException e) {
                    log.error("Ошибка в удалении пользователя");
                    log.error(e.getMessage());
                }
                break;
            case 3:
                try {
                    update();
                } catch (SQLException | IOException e) {
                    log.error("Ошибка в обновлении пользователя - " + e.getMessage());
                }
                break;
            case 4:
                try {
                    read();
                } catch (SQLException e) {
                    log.error("Ошибка в просмотре базы данных - " + e.getMessage());
                }
                break;
        }
    }

    /**
     * Command line interface for create account function
     * @throws IOException
     * @throws SQLException
     */
    public void create() throws IOException, SQLException {
        User currentUser = new User();
        log.info("Введите name, surname");
        String userInfo = bufferedReader.readLine();
        String[] userInfoList = userInfo.split(", ", 0);
        if (userInfoList.length > 1) {
            currentUser.setName(userInfoList[0]);
            currentUser.setSurname(userInfoList[1]);
            manager.createUser(currentUser.getName(), currentUser.getSurname());
        } else {
            throw new IOException("Не хватает данных");
        }
    }

    /**
     * Command line interface for read accounts function
     * @throws SQLException
     */
    public void read() throws SQLException {
        List<User> listUsers = manager.readUsers();
        for(Object object : listUsers) {
            User user = (User) object;
            log.info("Пользователь - id = " + user.getId() + ", имя = " +
                    user.getName() + ", фамилия = " + user.getSurname());
        }
    }

    /**
     * Command line interface for delete account function
     * @throws SQLException
     */
    public void delete() throws SQLException, IOException {
        log.info("Введите id ");
        int id = Integer.parseInt(bufferedReader.readLine());
        manager.deleteUser(id);
    }

    /**
     * Command line interface for update account function
     * @throws SQLException
     */
    public void update() throws SQLException, IOException {
        User currentUser = new User();
        log.info("Введите id, name, surname");
        String userInfo = bufferedReader.readLine();
        String[] userInfoList = userInfo.split(", ", 0);
        currentUser.setId(Integer.parseInt(userInfoList[0]));
        currentUser.setName(userInfoList[1]);
        currentUser.setSurname(userInfoList[2]);
        manager.updateUser(currentUser.getId(), currentUser.getName(),
                currentUser.getSurname());
    }

}
