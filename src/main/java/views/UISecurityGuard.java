package views;

import org.apache.log4j.Logger;
import services.SecurityGuard;

/**
 * Use to provide a SecurityGuard CLI
 * @author Anton Chekulaev
 * @version 1.0
 * @see services.SecurityGuard
 */
public class UISecurityGuard {

    /**  Logging  */
    private static final Logger log = Logger.getLogger(UISecurityGuard.class);

    /**  Classes composition with Manager and run a SecurityGuard constructor*/
    SecurityGuard guard = new SecurityGuard();

    /** Constructor - run a guard method, returning message */
    public UISecurityGuard() {
        log.info(guard.talk());
    }

}
