package model;

import java.io.Serializable;

/**
 * Account class with <b>userId</b>,
 * <b>accountNumber</b> and <b>accountMoney</b> properties
 * @author Anton Chekulaev
 * @version 1.0
 */
public class Account implements Serializable {

    /** user id field*/
    private int userId;

    /**
     * account number field
     * @serial field
     */
    private int accountNumber;

    /** account money field*/
    private double accountMoney;

    /** Method get value of {@link Account#userId} field
     * @return user id
     */
    public int getUserId() {
        return userId;
    }

    /** Method set value of {@link Account#userId} field*/
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /** Method get value of {@link Account#accountNumber} field
     * @return account number
     */
    public int getAccountNumber() {
        return accountNumber;
    }

    /** Method set value of {@link Account#accountNumber} field*/
    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    /** Method get value of {@link Account#accountMoney} field
     * @return money in the account
     */
    public double getAccountMoney() {
        return accountMoney;
    }

    /** Method set value of {@link Account#accountMoney} field*/
    public void setAccountMoney(double accountMoney) {
        this.accountMoney = accountMoney;
    }

    /**
     * Method leads the model to a string
     * @return String of account model
     */
    @Override
    public String toString()
    {
        return  userId + ", " + accountNumber + ", " + accountMoney;
    }
}
