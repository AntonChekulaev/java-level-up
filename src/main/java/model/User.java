package model;

import java.io.Serializable;
import java.util.List;

/**
 * User class with <b>id</b>,
 * <b>name</b>, <b>surname</b> and <b>account</b> properties
 * @author Anton Chekulaev
 * @version 1.0
 */
public class User implements Serializable {

    /** user id field
     * @serial field
     */
    private int id;

    /** user name field*/
    private String name;

    /** user surname field*/
    private String surname;

    /** all user's accounts field*/
    private List<Account> account;

    /** Method get value of {@link User#id} field
     * @return user id
     */
    public int getId() {
        return id;
    }

    /** Method set value of {@link User#id} field*/
    public void setId(int id) {
        this.id = id;
    }

    /** Method get value of {@link User#name} field
     * @return user name
     */
    public String getName() {
        return name;
    }

    /** Method set value of {@link User#name} field*/
    public void setName(String name) {
        this.name = name;
    }

    /** Method get value of {@link User#surname} field
     * @return user surname
     */
    public String getSurname() {
        return surname;
    }

    /** Method set value of {@link User#surname} field*/
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /** Method get value of {@link User#account} field
     * @return all user's accounts
     */
    public List<Account> getAccount() {
        return account;
    }

    /** Method set value of {@link User#account} field*/
    public void setAccount(List<Account> account) {
        this.account = account;
    }

    /**
     * Method leads the model to a string
     * @return String of user model
     */
    @Override
    public String toString()
    {
        return  id + ", " + name + ", " + surname;
    }


}
