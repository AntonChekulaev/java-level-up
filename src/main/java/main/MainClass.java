package main;

import org.apache.log4j.Logger;
import services.AccountManager;
import services.Manager;
import services.SecurityGuard;
import views.UIAccountManager;
import views.UIMain;
import views.UIManager;

import java.util.Scanner;

/**
 * Main class run the program
 * @author Anton Chekulaev
 * @version 1.0
 */
public class MainClass {

    /**
     * Method create UIMain object and use UIMain constructor for run the program
     * @see UIMain
     * @param args
     */
    public static void main(String args[]){
        UIMain main = new UIMain();
    }

}
