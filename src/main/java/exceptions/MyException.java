package exceptions;

import java.io.IOException;

/**
 * Class is a subclass of exception and MyException to message
 * @author Anton Chekulaev
 * @version 1.0
 */
public class MyException extends Exception {

    /** error message field */
    private String detail;

    /**
     * Method get a error message
     * @param a
     */
    public MyException(String a) {
        detail = a;
    }

    /**
     * Method return error with MyException postsctipt
     * @return modify error string
     */
    public String toString() {
        return "MyException! " + detail;
    }

}
